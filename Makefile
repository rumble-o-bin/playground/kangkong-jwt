CLUSTER_NAME="kind-infras"
CLUSTER_VERSION="kindest/node:v1.25.11"

.PHONY: help
help:  ## Show help
	@awk 'BEGIN {FS = ":.*##"; printf "\nUsage:\n  make \033[36m<target>\033[0m\n\nTargets:\n"} /^[a-zA-Z0-9_-]+:.*?##/ { printf "  \033[36m%-20s\033[0m %s\n", $$1, $$2 }' $(MAKEFILE_LIST)

start_cluster: ## Start kind cluster
	@kind create cluster --name $(CLUSTER_NAME) --config hack/config.yaml --image $(CLUSTER_VERSION)

delete_cluster: ## Delete cluster kind
	@kind delete cluster --name $(CLUSTER_NAME)

install_pg:
	@kubectl apply -k pg

install_kong:
	@helm upgrade --install kong chart/kong/ -f chart/kong/values.yaml --namespace kong
